import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardContainerComponent } from './dashboard-container/dashboard-container.component';
import { DashboardListComponent } from './dashboard-list/dashboard-list.component';
import { DashboardTileComponent } from './dashboard-tile/dashboard-tile.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgxsModule } from '@ngxs/store';
import { TileState } from './data/tile.state';



@NgModule({
  declarations: [
    DashboardContainerComponent,
    DashboardListComponent,
    DashboardTileComponent,
  ],
  imports: [
    CommonModule,
    NgxsModule.forFeature([TileState]),
    DashboardRoutingModule,
  ]
})
export class DashboardModule { }
