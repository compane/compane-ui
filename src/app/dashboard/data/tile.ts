import { Widget } from './widget';

export interface Tile {
    id: number;
    boardId: number;
    widgetId: number;
    widget: Widget;
    order: number;
}