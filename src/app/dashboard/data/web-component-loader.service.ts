import { Injectable, Injector, NgModuleFactoryLoader, NgModuleRef, NgModuleFactory } from '@angular/core';
import { Widget } from './widget';

@Injectable({
    providedIn: 'root'
})
export class WebComponentLoaderService {

    private loadedComponents: Set<string> = new Set<string>([]);

    constructor(
        private loader: NgModuleFactoryLoader,
        private injector: Injector
    ) {
    }

    loadLazyComponent(widget: Widget): Promise<void> {
        if (this.loadedComponents.has(widget.tag)) {
            return Promise.resolve();
        }

        return this
            .loader
            .load(widget.reference)
            .then(moduleFactory => {
                const moduleRef = moduleFactory.create(this.injector).instance;
                this.loadedComponents.add(widget.tag);
                console.debug('moduleRef', moduleRef);
            })
            .catch(err => {
                console.error('error loading module', err);
            });

    }

    loadExternalComponent(widget: Widget): void {
        if (this.loadedComponents.has(widget.tag)) {
            return;
        }

        const script = document.createElement('script');
        script.src = widget.reference;
        document.body.appendChild(script);

        this.loadedComponents.add(widget.tag);
    }
}
