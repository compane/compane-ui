import { Tile } from './tile';

export class AddTile {
    static readonly type = '[Tile] Add';

    constructor(public payload: Tile) {
    }
}

export class GetTiles {
    static readonly type = '[Tile] Get All';
}

export class UpdateTile {
    static readonly type = '[Tile] Update';

    constructor(public payload: Tile, public id: number) {
    }
}

export class DeleteTile {
    static readonly type = '[Tile] Delete';

    constructor(public id: number) {
    }
}