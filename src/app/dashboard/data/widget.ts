export interface Widget {
    id: number;
    title: string;
    type: string;
    reference: string;
    tag: string;
    //TODO add width
}