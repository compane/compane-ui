import { Injectable } from '@angular/core';
import { BasicCrudService } from '../../shared/basic-crud-service';
import { Tile } from './tile';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TileService extends BasicCrudService<Tile> {
  protected baseUrl: string = 'api/tiles';
  
  constructor(protected http: HttpClient) {
    super(http);
  }
}
