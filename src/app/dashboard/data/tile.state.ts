import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Tile } from './tile';
import { AddTile, DeleteTile, GetTiles, UpdateTile } from './tile.action';
import { TileService } from './tile.service';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

export class TileStateModel {
    tiles: Tile[];
}

@State<TileStateModel>({
    name: 'tiles',
    defaults: {
        tiles: []
    }
})
@Injectable()
export class TileState {

    constructor(private tileService: TileService) {
    }

    @Selector()
    static getTileList(state: TileStateModel) {
        return state.tiles;
    }

    @Action(GetTiles)
    getTiles({ getState, setState }: StateContext<TileStateModel>) {
        return this.tileService.readAll().pipe(tap((result) => {
            const state = getState();
            setState({
                ...state,
                tiles: result,
            });
        }));
    }

    @Action(AddTile)
    addTile({ getState, patchState }: StateContext<TileStateModel>, { payload }: AddTile) {
        return this.tileService.create(payload).pipe(tap((result) => {
            const state = getState();
            patchState({
                tiles: [...state.tiles, result]
            });
        }));
    }

    @Action(UpdateTile)
    updateTile({ getState, setState }: StateContext<TileStateModel>, { payload, id }: UpdateTile) {
        return this.tileService.update(payload, id).pipe(tap((result) => {
            const state = getState();
            const tileList = [...state.tiles];
            const tileIndex = tileList.findIndex(item => item.id === id);
            tileList[tileIndex] = result;
            setState({
                ...state,
                tiles: tileList,
            });
        }));
    }


    @Action(DeleteTile)
    deleteTile({ getState, setState }: StateContext<TileStateModel>, { id }: DeleteTile) {
        return this.tileService.delete(id).pipe(tap(() => {
            const state = getState();
            const filteredArray = state.tiles.filter(item => item.id !== id);
            setState({
                ...state,
                tiles: filteredArray,
            });
        }));
    }

    /*@Action(SetSelectedTile)
    setSelectedTileId({ getState, setState }: StateContext<TileStateModel>, { payload }: SetSelectedTile) {
        const state = getState();
        setState({
            ...state,
            selectedTile: payload
        });
    }*/
}
