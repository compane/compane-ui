import { Component, OnInit, Input } from '@angular/core';
import { Tile } from '../data/tile';

@Component({
  selector: 'app-dashboard-list',
  templateUrl: './dashboard-list.component.html',
  styleUrls: ['./dashboard-list.component.css']
})
export class DashboardListComponent implements OnInit {

  @Input() tiles: Tile[];
  
  constructor() { }

  ngOnInit(): void {
  }

}
