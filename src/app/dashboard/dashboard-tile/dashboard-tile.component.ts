import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { Tile } from '../data/tile';
import { WebComponentLoaderService } from '../data/web-component-loader.service';

@Component({
  selector: 'app-dashboard-tile',
  templateUrl: './dashboard-tile.component.html',
  styleUrls: ['./dashboard-tile.component.css']
})
export class DashboardTileComponent implements AfterViewInit {

  @Input() tile: Tile;
  @ViewChild('widgetContainer') widgetContainer: ElementRef;


  constructor(
    private renderer:Renderer2,
    private webComponentLoader: WebComponentLoaderService
    ) {
  }

  ngAfterViewInit(): void {
    if (this.tile.widget.type === "external") {
      this.webComponentLoader.loadExternalComponent(this.tile.widget);
    } else if (this.tile.widget.type === "lazy") {
      this.webComponentLoader.loadLazyComponent(this.tile.widget);
    }

    this.setupTile(this.tile.widget.tag);
  }

  setupTile(tagName: string) {
    const widget = this.renderer.createElement(tagName);
    widget.setAttribute('name', '121230932109');
    this.renderer.appendChild(this.widgetContainer.nativeElement, widget);
    // tile.setAttribute('a', '' + data[0]);
    // tile.setAttribute('b', '' + data[1]);
    // tile.setAttribute('c', '' + data[2]);
  }
}
