import { Component, OnInit } from '@angular/core';
import { TileState } from '../data/tile.state';
import { Tile } from '../data/tile';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetTiles } from '../data/tile.action';

@Component({
  selector: 'app-dashboard-container',
  templateUrl: './dashboard-container.component.html',
  styleUrls: ['./dashboard-container.component.css']
})
export class DashboardContainerComponent implements OnInit {


  @Select(TileState.getTileList) tiles$: Observable<Tile[]>;
  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(new GetTiles());
  }
}
