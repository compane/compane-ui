import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError, map } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

export abstract class BasicCrudService<T> {

    protected abstract baseUrl: string;

    constructor(protected http: HttpClient) { }

    public readAll(): Observable<T[]> {
        return this.http.get<T[]>(this.baseUrl)
            .pipe(
                tap(data => console.log(JSON.stringify(data))),
                catchError(this.handleError)
            );
    }

    public read(id: number): Observable<T> {
        const url = `${this.baseUrl}/${id}`;

        return this.http.get<T>(this.baseUrl)
            .pipe(
                tap(data => console.log(JSON.stringify(data))),
                catchError(this.handleError)
            );
    }

    public create(obj: T): Observable<T> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        // Project Id must be null for the Web API to assign an Id
        //const newProject = { ...obj, id: null };
        return this.http.post<T>(this.baseUrl, obj, { headers })
            .pipe(
                tap(data => console.log('createProject: ' + JSON.stringify(data))),
                catchError(this.handleError)
            );
    }

    public update(obj: T, id: number): Observable<T> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = `${this.baseUrl}/${id}`;

        return this.http.put<T>(url, obj, { headers })
            .pipe(
                tap(() => console.log('updateProject: '/* + obj.id*/)),
                // Return the project on an update
                map(() => obj),
                catchError(this.handleError)
            );
    }

    public delete(id: number): Observable<{}> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = `${this.baseUrl}/${id}`;

        return this.http.delete<T>(url, { headers })
            .pipe(
                tap(data => console.log('deleteProject: ' + id)),
                catchError(this.handleError)
            );
    }

    private handleError(err) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
        }
        console.error(err);
        return throwError(errorMessage);
    }
}
