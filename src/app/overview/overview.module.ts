import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverviewContainerComponent } from './overview-container/overview-container.component';
import { BoardDetailComponent } from './board-detail/board-detail.component';
import { BoardFormComponent } from './board-form/board-form.component';
import { BoardListComponent } from './board-list/board-list.component';
import { NgxsModule } from '@ngxs/store';
import { BoardState } from './data/board.state';
import { OverviewRoutingModule } from './overview-routing.module';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    OverviewContainerComponent,
    BoardDetailComponent,
    BoardFormComponent,
    BoardListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forFeature([BoardState]),
    OverviewRoutingModule,
  ]
})
export class OverviewModule { }
