import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Board } from '../data/board';

@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.css']
})
export class BoardDetailComponent implements OnInit {
  @Input() board: Board;

  @Output() edit = new EventEmitter<number>();
  @Output() delete = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  editBoard() {
    this.edit.emit(this.board.id);
  }

  deleteBoard() {
    if (this.board && this.board.id) {
      if (confirm(`Do you really want to delete the board: ${this.board.name}?`)) {
        this.delete.emit(this.board.id);
      }
    }
  }
}
