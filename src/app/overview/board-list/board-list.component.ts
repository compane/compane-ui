import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Board } from '../data/board';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.css']
})
export class BoardListComponent implements OnInit {
  @Input() boards: Board[];

  isBeingEdited: Set<number> = new Set();
  @Output() update = new EventEmitter<Board>();
  @Output() delete = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  editBoard(id: number) {
    this.isBeingEdited.add(id);
  }

  deleteBoard(id: number) {
    this.delete.emit(id);
  }

  cancelEdit(id: number) {
    this.isBeingEdited.delete(id);
  }

  saveBoard(board: Board) {
    this.update.emit(board);
    this.cancelEdit(board.id);
  }
}
