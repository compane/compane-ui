import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Board } from '../data/board';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-board-form',
  templateUrl: './board-form.component.html',
  styleUrls: ['./board-form.component.css']
})
export class BoardFormComponent implements OnInit {
  @Input() board: Board;
  @Output() cancel = new EventEmitter<number>();
  @Output() save = new EventEmitter<Board>();
  boardForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.boardForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      description: [''],
      color: ['accent-1']
    });
  }

  ngOnInit(): void {
    this.boardForm.patchValue({
      id: this.board.id,
      name: this.board.name,
      description: this.board.description,
      color: this.board.color,
    });
  }
  
  cancelEdit(): void {
    this.cancel.emit(this.board.id)
  }
  
  onSubmit(): void {
    this.save.emit(this.boardForm.value)
  }
}
