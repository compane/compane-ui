export interface Board {
    id: number;
    userId: number;
    name: string;
    description: string;
    color: string;
    icons: string[];
}