import { Board } from './board';

export class AddBoard {
    static readonly type = '[Board] Add';

    constructor(public payload: Board) {
    }
}

export class GetBoards {
    static readonly type = '[Board] Get All';
}

export class UpdateBoard {
    static readonly type = '[Board] Update';

    constructor(public payload: Board, public id: number) {
    }
}

export class DeleteBoard {
    static readonly type = '[Board] Delete';

    constructor(public id: number) {
    }
}