import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Board } from './board';
import { AddBoard, DeleteBoard, GetBoards, UpdateBoard } from './board.action';
import { BoardService } from './board.service';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

export class BoardStateModel {
    boards: Board[];
}

@State<BoardStateModel>({
    name: 'boards',
    defaults: {
        boards: []
    }
})
@Injectable()
export class BoardState {

    constructor(private boardService: BoardService) {
    }

    @Selector()
    static getBoardList(state: BoardStateModel) {
        return state.boards;
    }

    @Action(GetBoards)
    getBoards({ getState, setState }: StateContext<BoardStateModel>) {
        return this.boardService.readAll().pipe(tap((result) => {
            const state = getState();
            setState({
                ...state,
                boards: result,
            });
        }));
    }

    @Action(AddBoard)
    addBoard({ getState, patchState }: StateContext<BoardStateModel>, { payload }: AddBoard) {
        return this.boardService.create(payload).pipe(tap((result) => {
            const state = getState();
            patchState({
                boards: [...state.boards, result]
            });
        }));
    }

    @Action(UpdateBoard)
    updateBoard({ getState, setState }: StateContext<BoardStateModel>, { payload, id }: UpdateBoard) {
        return this.boardService.update(payload, id).pipe(tap((result) => {
            const state = getState();
            const boardList = [...state.boards];
            const boardIndex = boardList.findIndex(item => item.id === id);
            boardList[boardIndex] = result;
            setState({
                ...state,
                boards: boardList,
            });
        }));
    }


    @Action(DeleteBoard)
    deleteBoard({ getState, setState }: StateContext<BoardStateModel>, { id }: DeleteBoard) {
        return this.boardService.delete(id).pipe(tap(() => {
            const state = getState();
            const filteredArray = state.boards.filter(item => item.id !== id);
            setState({
                ...state,
                boards: filteredArray,
            });
        }));
    }

    /*@Action(SetSelectedBoard)
    setSelectedBoardId({ getState, setState }: StateContext<BoardStateModel>, { payload }: SetSelectedBoard) {
        const state = getState();
        setState({
            ...state,
            selectedBoard: payload
        });
    }*/
}
