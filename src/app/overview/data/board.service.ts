import { Injectable } from '@angular/core';
import { BasicCrudService } from '../../shared/basic-crud-service';
import { Board } from './board';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BoardService extends BasicCrudService<Board> {
  protected baseUrl: string = 'api/boards';
  
  constructor(protected http: HttpClient) {
    super(http);
  }
}
