import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BoardState } from '../data/board.state';
import { GetBoards, DeleteBoard, UpdateBoard } from '../data/board.action';
import { Board } from '../data/board';

@Component({
  selector: 'app-overview-container',
  templateUrl: './overview-container.component.html',
  styleUrls: ['./overview-container.component.css']
})
export class OverviewContainerComponent implements OnInit {

  @Select(BoardState.getBoardList) boards$: Observable<Board[]>;
  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(new GetBoards());
  }

  updateBoard(board: Board) {
    this.store.dispatch(new UpdateBoard(board, board.id));
  }

  deleteBoard(id: number) {
    this.store.dispatch(new DeleteBoard(id));
  }
}
