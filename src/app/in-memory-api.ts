
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Tile } from './dashboard/data/tile';
import { Board } from './overview/data/board';

export class InMemoryApi implements InMemoryDbService {
    createDb() {
        let tiles: Tile[] = [
            {
                id: 1,
                boardId: 1,
                widgetId: 1,
                widget: {
                    id: 1,
                    title: "Gmail",
                    type: "external",
                    reference: "http://localhost:5001/main.js",
                    tag: "gmail-widget",
                },
                order: 1,
            },
            {
                id: 2,
                boardId: 1,
                widgetId: 1,
                widget: {
                    id: 1,
                    title: "Gmail",
                    type: "external",
                    reference: "http://localhost:5001/main.js",
                    tag: "gmail-widget",
                },
                order: 1,
            },
        ];

        let boards: Board[] = [
            {
                id: 1,
                userId: 1,
                name: "Work",
                description: "Board to manage all work related stuff",
                color: "accent-1",
                icons: [
                    "https://i.pinimg.com/originals/31/23/9a/31239a2f70e4f8e4e3263fafb00ace1c.png",
                    "https://cdn.icon-icons.com/icons2/836/PNG/512/Spotify_icon-icons.com_66783.png",
                    "https://image.flaticon.com/icons/png/512/281/281769.png",
                ],

            },
            {
                id: 2,
                userId: 1,
                name: "School",
                description: "Board to manage all school related stuff",
                color: "accent-2",
                icons: [
                    "https://upload.wikimedia.org/wikipedia/commons/d/da/Google_Drive_logo.png",
                    "https://is4-ssl.mzstatic.com/image/thumb/Purple113/v4/8a/ce/d4/8aced464-b666-cdf4-393d-58f9bd38f992/OneDrive.png/246x0w.png",
                    "https://image.flaticon.com/icons/png/512/281/281769.png",
                ],

            },
            {
                id: 3,
                userId: 1,
                name: "Private",
                description: "Keep out. Members only.",
                color: "accent-5",
                icons: [
                    "https://cdn.icon-icons.com/icons2/836/PNG/512/Spotify_icon-icons.com_66783.png",
                    "https://stagewp.sharethis.com/wp-content/uploads/2017/05/Reddit.png",
                    "https://i.pinimg.com/originals/31/23/9a/31239a2f70e4f8e4e3263fafb00ace1c.png",
                    "https://miro.medium.com/proxy/1*6bqgBkbNo7kXLv2qXU6NHQ.jpeg",
                ],

            },
        ];

        return { boards, tiles };
    }
}

/*
{
    id: 2,
    title: "Todo",
    type: "lazy",
    reference: "src/app/todo/todo.module#TodoModule",
    tag: "todo-widget",
},
{
    id: 3,
    title: "Note",
    type: "internal",
    reference: "",
    tag: "note-widget",
},
*/